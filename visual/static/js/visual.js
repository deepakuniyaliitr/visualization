$("#generate-chart").click(function () { 
$.getJSON('http://127.0.0.1:8000/charts/api-movies/?format=json', function (data) {
    var jsonData = {};
    var propertyIndex = $("#chart-template option:selected").index();
    var property = "";

    switch(propertyIndex){
        case 0:
            property = "genere";
            break;
        case 1:
            property = "actor";
            break;
        case 2:
            property = "actress";
            break;
        case 3:
            property = "director";
            break;
        case 4:
            property = "rating";
            break;
        case 5:
            property = "release_year";
            break;
        case 6:
            property = "running_time";
            break;
        default:
            property = "genere";
            break;
    }

    if(property=="release_year"){
        for (var i =0 ; i < data.length; i++){
            if (jsonData.hasOwnProperty(data[i][property].substring(0, 4))){
                jsonData[data[i][property].substring(0, 4)] += 1
                }
            else{
                jsonData[data[i][property].substring(0, 4)] = 1
                }
            }
    }
    else{
        for (var i =0 ; i < data.length; i++){
            if (jsonData.hasOwnProperty(data[i][property])){
                jsonData[data[i][property]] += 1
                }
            else{
                jsonData[data[i][property]] = 1
                }
            }
    }

    var length = jsonData.length;
    var categories = Object.keys(jsonData);
    var count = [];

    for(var i=0 ; i < categories.length; i++){
        count.push(jsonData[categories[i]]);
    }

    $('#chart-container').highcharts({
        chart: {
            type: $("#chart-type option:selected").val()
        },
        title: {
            text: 'Movies Visualization'
        },
        xAxis: {
            title: {
                enabled: true,
                text: property
            },
            categories: categories
        },
        yAxis: {
            title: {
                text: $("#chart-template option:selected").val()
            }
        },
        series: [
            {
                data : count
            }
        ]
    });
});
});
