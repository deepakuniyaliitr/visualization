# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-26 09:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('charts', '0003_auto_20160226_0817'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='movies',
            name='release_year',
        ),
    ]
