from rest_framework import serializers
from .models import Movies


class MoviesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movies
        field = ('id', 'title', 'actor', 'actress', 'genere', 'director', 'running_time', 'release_year', 'rating')
