from django.shortcuts import render_to_response
from rest_framework import generics
from .serializers import MoviesSerializer
from .models import Movies
from django.http import HttpResponse, HttpResponseRedirect
from django.core.context_processors import csrf

def ChartsView(request):
    args = {}
    args.update(csrf(request))

    args['movies'] = Movies.objects.all()
    return render_to_response('charts/charts.html',args)

class MoviesList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of movies.
    """
    queryset = Movies.objects.all()
    model = Movies
    serializer_class = MoviesSerializer


class MoviesDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single movie.
    """
    model = Movies
    queryset = Movies.objects.all()
    serializer_class = MoviesSerializer
