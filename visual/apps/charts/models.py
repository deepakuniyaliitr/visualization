from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Movies(models.Model):
    title = models.CharField(max_length=200)
    actor = models.CharField(max_length=200)
    actress = models.CharField(max_length=200)
    genere = models.CharField(max_length=200)
    director = models.CharField(max_length=200)
    running_time = models.IntegerField(default=0)
    release_year  = models.DateField('Release Year')
    rating = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(10)], verbose_name="Rating")
    
    def __str__(self):
        return self.title
