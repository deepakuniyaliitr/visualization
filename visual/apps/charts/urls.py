from django.conf.urls import url
from .views import MoviesDetail, MoviesList
from . import views

urlpatterns = [
    url(r'^$', views.ChartsView, name='index'),
    url(r'^api-movies/$', MoviesList.as_view(),name='api-movies-list'),
    url(r'api-movie-details/(?P<pk>\d+)/$', MoviesDetail.as_view(),name='api-movies-detail'),
]